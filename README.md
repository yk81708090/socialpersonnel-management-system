# vue+nodejs+mysql 社团人员管理系统

#### 介绍
该项目采用的是前后端分离的架构，前端采用的是vue框架和elementUI，后端用得是node.js，使用的数据库mysql 。该项目在登录时使用了token验证，设置路由导航守卫对路由的跳转进行判断、md5加密，还有对人员信息进行增删改查等数据操作

#### 软件架构
软件架构说明
前端采用的是vue框架和elementUI，后端用得是node.js，使用的数据库mysql


#### 安装教程
1.  下载phpstudy，启动mysql，创建数据库
2.  下载Navicat，连接创建的数据库，建立查询，执行aaa.sql文件
3.  进入server目录/module/db.js文件下，修改数据库配置
4.  server目录下 npm i 下载依赖包
5.  server目录下 nodemon app.js运行程序
6.  client目录下 npm i 下载依赖包
7.  client目录下 npm run serve 运行程序
#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
