const express = require('express')
const con = require('../modul/db.js')
const jwt = require('jsonwebtoken')
var hash = require('object-hash');

// MD5

const router = express.Router()



// 处理数据的函数
// data 数据
// root 顶级数据
let getChildren = function (data, root) {
  var children = [];
  for (var i = 0; i < data.length; i++) {
    if (root == data[i].super) {
      data[i].children = getChildren(data, data[i].id);
      children.push(data[i]);
    }
  }
  return children;
}

// 获取所有部门信息
router.get('/getsection', (req, res) => {
  let sql = `SELECT * FROM section`
  con.sqlConnect(sql, (err, results) => {
    if (err) {
      console.log(err)
    } else {
      res.send(getChildren(results, 0))
    }
  })

})


// 根据部门id请求对应部门员工信息  参数 部门id
router.get('/getstaff', (req, res) => {
  let id = req.query.id
  let sql = `SELECT * FROM staff WHERE staff.seid = '${id}'`

  con.sqlConnect(sql, (err, results, fields) => {
    res.send(results)
  })

})


// 登录
router.post('/login', (req, res) => {
  const name = req.body.username;
  const password = req.body.password;
  possword = hash(password, {
    algorithm: "md5",
    encoding: "base64",
  });


  let sql = `select * from userInfo where name='${name}'`
  con.sqlConnect(sql, (err, data1) => {
    if (!data1.length) {
      return res.send({
        code: 400,
        msg: '用户不存在'
      })
    }
    else {
      // 查看密码是否匹配

      const token = jwt.sign({ name }, 'hui')

      let sql = `select * from userInfo where password='${password}' and  name='${name}'`
      con.sqlConnect(sql, (err, data) => {
        if (!data.length) {
          return res.send({
            code: 400,
            msg: '登录密码错误'
          })
        }
        console.log('data' + data);
        res.send({
          token,
          name: name,
          code: 200,
          msg: '登录成功',
          data: data

        })


      })
    }

  })


})


// 注册
router.post('/register', (req, res) => {
  console.log(req.body);
  const name = req.body.username;
  console.log(name);


  const password = req.body.pass;

  const sex = req.body.sex;
  const age = req.body.age;
  const poid = req.body.poid;
  const birthday = req.body.birthday;
  const limits = req.body.limits;
  const other = req.body.other;




  // 向数据库添加注册数据
  let sql = `insert into userInfo (name,password,sex,age,poid,birthday,limits,other) values ('${name}','${password}','${sex}','${age}','${poid}','${birthday}','${limits}','${other}')`
  con.sqlConnect(sql, (err, result) => {
    if (err) {
      console.log(err);
      return res.send({
        code: 400,
        msg: "添加失败"
      })
    } else {
      res.send({
        code: 200,
        msg: "添加成功"
      })
    }

  })
})


// 获取用户userInfo
router.get('/getuserinfo', (req, res) => {

  var id = req.query.id
  console.log(id);

  let sql = `select * from userInfo where id='${id}'`
  con.sqlConnect(sql, (err, results, fields) => {
    console.log('result' + results);
    res.send(results)
  })
})

// 获得commonuser
router.get('/getcommonuser', (req, res) => {
  let sql = `select * from userInfo where limits='false'`
  con.sqlConnect(sql, (err, results, fields) => {
    console.log('result' + results);
    res.send(results)
  })
})

// 获得commonuser
router.get('/getsuperuser', (req, res) => {
  let sql = `select * from userInfo where limits='true'`
  con.sqlConnect(sql, (err, results, fields) => {
    console.log('result' + results);
    res.send(results)
  })
})





// 获取岗位信息
router.get('/getpost', (req, res) => {
  let sql = `SELECT * FROM post`
  con.sqlConnect(sql, (err, results, fields) => {
    res.send(results)
  })


})

//// 添加数据
router.post('/addstaff', (req, res) => {
  let sql = `INSERT INTO staff (name,sex,birthday,other,seid,poid) values ('${req.body.name}','${req.body.sex}','${req.body.birthday}','${req.body.other}',${req.body.seid},${req.body.poid})`
  con.sqlConnect(sql, (err, result) => {
    if (err) {
      console.log(err)
    } else {
      res.json({
        code: 200
      })
    }



  })
})

// 搜索员工信息
router.get('/findstaff', (req, res) => {
  let seid = req.query.seid //部门id
  let poid = req.query.poid //岗位id
  let name = req.query.name //员工姓名
  if (poid) {
    sql = `SELECT * FROM staff WHERE staff.poid = '${poid}' AND staff.seid = ${seid} and staff.name LIKE '%${name}%';`
  } else {
    sql = `SELECT * FROM staff WHERE  staff.seid = ${seid} and staff.name LIKE '%${name}%';`
  }
  con.sqlConnect(sql, (err, results, fields) => {
    console.log(results)
    res.send(results)
  })


})

// 删除人员

router.get('/removestaff', (req, res) => {
  let id = req.query.id
  let sql = `DELETE FROM staff WHERE id = '${id}'`
  con.sqlConnect(sql, (err, result) => {
    if (err) {
      console.log(err)
    } else {
      res.json({
        code: 200
      })
    }
  })

})


// 按人员id获取人员
router.get('/staff_id', (req, res) => {
  let id = req.query.id
  let sql = `SELECT * FROM staff WHERE staff.id = ${id} `

  con.sqlConnect(sql, (err, result) => {
    if (err) {
      console.log(err)
    } else {
      res.send(result)
    }
  })

})

// 更新 人员信息
router.post('/update', (req, res) => {
  let id = req.body.id

  let sta = req.body

  let newSta = [sta.name, sta.sex, sta.birthday, sta.other, sta.seid, sta.poid]
  // let sql = `UPDATE staff SET name = ?, sex = ?, birthday = ?, other = ?, seid = ?, poid = ? WHERE id = ${id}`
  let sql = `UPDATE staff SET name = '${sta.name}', sex = '${sta.sex}', birthday = '${sta.birthday}', other = '${sta.other}', seid = '${sta.seid}', poid = '${sta.poid}' WHERE id = ${id}`
  con.sqlConnect(sql, (err, result) => {
    if (err) {
      console.log(err)
    } else {
      res.json({
        code: 200
      })
    }
  })

})



module.exports = router