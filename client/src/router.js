import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import show from './components/show.vue'
import login from './views/Login'
import register from './views/Register'
import myself from './components/Myself'
import common from './components/Common'
import u from './components/suser'

import welcome from './components/Welcome'
// import { component } from 'vue/types/umd'
// import { component } from 'vue/types/umd'
Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [

    {
      path: '/',
      redirect: '/login'

    },
    // 登录路由
    {
      path: '/login',
      component: login
    },
    {
      path: '/register',
      component: register

    },
    //主页路由
    {
      path: '/home',
      component: Home,
      redirect: '/welcome',
      children: [
        {
          path: '/welcome',
          component: welcome
        },
        {
          path: '/myself/:id',
          component: myself
        },
        {
          path: '/show/:id',
          name: 'show',
          component: show,
        }, {
          path: '/common',
          component: common
        }, {
          path: '/common',
          component: common
        }, {
          path: '/super',
          component: u
        }]
    }


  ]
})

// 导航守卫验证token
router.beforeEach((to, from, next) => {
  if (to.path === '/login') return next()
  if (to.path === '/register') return next()
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})

export default router