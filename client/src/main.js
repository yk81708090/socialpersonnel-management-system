import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router'
import router from './router'
import './assets/css/reset.css'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import { Message } from 'element-ui';
Vue.prototype.$message = Message

import axios from 'axios'
// axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'

axios.defaults.baseURL = 'http://localhost:3000/'
// axios请求拦截
axios.interceptors.request.use(config => {
  // 为请求头对象，添加Token验证的Authorization字段
  config.headers.Authorization = window.sessionStorage.getItem('token')
  return config
})

Vue.prototype.axios = axios;

Vue.config.productionTip = false

Vue.use(ElementUI);




const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)

}

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
