/*
Navicat MySQL Data Transfer

Source Server         : usersql
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : aaa

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2021-11-07 14:45:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `post`
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of post
-- ----------------------------
INSERT INTO `post` VALUES ('1', '会长');
INSERT INTO `post` VALUES ('2', '部长');
INSERT INTO `post` VALUES ('3', '副部长');
INSERT INTO `post` VALUES ('4', '干事');
INSERT INTO `post` VALUES ('5', '普通人员');

-- ----------------------------
-- Table structure for `section`
-- ----------------------------
DROP TABLE IF EXISTS `section`;
CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `super` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of section
-- ----------------------------
INSERT INTO `section` VALUES ('1', '学生会', '0');
INSERT INTO `section` VALUES ('2', '校青协', '0');
INSERT INTO `section` VALUES ('3', '语言培训社', '0');
INSERT INTO `section` VALUES ('4', '外宣部', '1');
INSERT INTO `section` VALUES ('5', '新闻部', '1');
INSERT INTO `section` VALUES ('6', '组织部', '1');
INSERT INTO `section` VALUES ('7', '组织部', '2');
INSERT INTO `section` VALUES ('8', '秘书处', '2');
INSERT INTO `section` VALUES ('9', '外宣部', '3');

-- ----------------------------
-- Table structure for `staff`
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `other` varchar(255) DEFAULT NULL,
  `seid` int(11) DEFAULT NULL,
  `poid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=200529043 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of staff
-- ----------------------------
INSERT INTO `staff` VALUES ('200529037', '张三', '女', '2021-10-12', '', '5', '5');
INSERT INTO `staff` VALUES ('200529032', '貂蝉', '女', '2015-06-02', '123', '5', '2');
INSERT INTO `staff` VALUES ('200529005', '贾诩', '女', '1984-06-12', null, '6', '3');
INSERT INTO `staff` VALUES ('200529007', '曹操', '男', '1987-01-07', 'null', '8', '4');
INSERT INTO `staff` VALUES ('200529008', '小乔', '女', '1988-10-26', null, '8', '2');
INSERT INTO `staff` VALUES ('200529035', '张三', '男', '2021-10-14', '', '7', '3');
INSERT INTO `staff` VALUES ('200529010', '孙尚香', '女', '2008-08-08', null, '9', '4');
INSERT INTO `staff` VALUES ('200529011', '貂蝉', '女', '1945-11-30', null, '7', '1');
INSERT INTO `staff` VALUES ('200529012', '孙万青', '男', '2020-05-06', '', '2', '4');
INSERT INTO `staff` VALUES ('200529039', '拉拉', '女', '2021-09-26', '', '4', '2');
INSERT INTO `staff` VALUES ('200529014', '庞统', '男', '2013-03-19', '凤雏', '5', '5');
INSERT INTO `staff` VALUES ('200529015', '周瑜', '女', '2009-05-11', '东风不与周郎便，铜雀春深锁二乔', '5', '4');
INSERT INTO `staff` VALUES ('200529016', '码云', '男', '2020-05-26', '123', '5', '1');
INSERT INTO `staff` VALUES ('200529017', '我没收到', '男', '2020-05-04', '123', '5', '4');
INSERT INTO `staff` VALUES ('200529038', '惠子', '女', '2021-10-18', '', '4', '4');
INSERT INTO `staff` VALUES ('200529019', '123213', '男', '2020-05-13', '123', '4', '3');
INSERT INTO `staff` VALUES ('200529020', '什么大气', '男', '2020-05-05', '123', '5', '4');
INSERT INTO `staff` VALUES ('200529021', '打磨按上面的', '女', '2020-05-04', '', '4', '4');
INSERT INTO `staff` VALUES ('200529022', '打磨按上面的', '女', '2020-05-04', '', '4', '4');
INSERT INTO `staff` VALUES ('200529023', '请问', '女', '2020-05-05', '', '5', '2');
INSERT INTO `staff` VALUES ('200529024', '123123', '男', '2020-05-12', '', '4', '5');
INSERT INTO `staff` VALUES ('200529025', '2131232', '男', '2020-05-12', '', '4', '5');
INSERT INTO `staff` VALUES ('200529026', '年末文', '男', '2020-05-05', '123', '9', '1');
INSERT INTO `staff` VALUES ('200529027', '13231232', '男', '2020-05-19', '2', '9', '3');
INSERT INTO `staff` VALUES ('200529028', '13231232', '男', '2020-05-19', '2', '9', '3');
INSERT INTO `staff` VALUES ('200529029', '马而我却二', '男', '2020-04-28', '', '6', '2');
INSERT INTO `staff` VALUES ('200529033', '周杰伦', '女', '2020-05-05', '稻香', '5', '4');
INSERT INTO `staff` VALUES ('200529040', '张白白', '男', '2021-10-12', '天真无邪', '6', '5');
INSERT INTO `staff` VALUES ('200529041', '小明', '男', '2021-11-02', '测', '4', '1');
INSERT INTO `staff` VALUES ('200529042', '张晓晓', '女', '2021-11-09', '我是好学生', '5', '5');

-- ----------------------------
-- Table structure for `userinfo`
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `password` varchar(255) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `age` int(10) NOT NULL,
  `poid` varchar(10) NOT NULL,
  `birthday` datetime(6) DEFAULT NULL,
  `limits` varchar(10) NOT NULL,
  `other` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=202112 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userinfo
-- ----------------------------
INSERT INTO `userinfo` VALUES ('1', 'zhang', '123456', '男', '5', '5', '2021-09-27 00:00:00.000000', 'true', '');
INSERT INTO `userinfo` VALUES ('2', 'zhan', '123456', '男', '5', '1', '2021-10-18 00:00:00.000000', 'false', '');
INSERT INTO `userinfo` VALUES ('3', 'zh', '123456', '女', '5', '3', '2021-10-12 00:00:00.000000', 'true', '');
INSERT INTO `userinfo` VALUES ('4', 'hui', '123456', '男', '21', '2', '2021-10-12 00:00:00.000000', 'true', '');
INSERT INTO `userinfo` VALUES ('5', '例子', '123', '女', '26', '2', '2021-10-05 00:00:00.000000', 'false', '');
INSERT INTO `userinfo` VALUES ('6', 'asdkfjb ', 'ddSWa7BJ9zeC5ukX5YXCbQ==', '男', '12', '2', '2021-11-15 00:00:00.000000', 'false', '');
INSERT INTO `userinfo` VALUES ('7', 'sssss', 'VQkI4YnEYjsrudGqHm4tJw==', '男', '56', '3', '2021-11-16 00:00:00.000000', 'true', '');
INSERT INTO `userinfo` VALUES ('8', '辉', 'FMj2apQbAa7A+BYFZZtFCg==', '男', '15', '2', '2021-11-22 00:00:00.000000', 'false', '');
INSERT INTO `userinfo` VALUES ('9', 'admin', 'FMj2apQbAa7A+BYFZZtFCg==', '男', '15', '2', '2021-11-14 00:00:00.000000', 'false', '');
INSERT INTO `userinfo` VALUES ('202111', '小明', 'FMj2apQbAa7A+BYFZZtFCg==', '男', '66', '2', '2021-11-09 00:00:00.000000', 'true', '');
